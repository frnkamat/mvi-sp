# Task:

Task is described in report.pdf and at [the competition website](https://www.kaggle.com/competitions/optiver-trading-at-the-close)

# Data
The data isn't uploaded to the repository, it can be downloaded [here](https://www.kaggle.com/competitions/optiver-trading-at-the-close/data?select=train.csv). Only the train.csv file is needed.

# How to run

Different notebooks have different requirements. To be able to run everything, you will need:
- python3
- pandas
- numpy
- jupyter
- matplotlib
- seaborn
- tqdm
- sklearn
- scipy
- tensorflow
- numba


