import pickle
from typing import Tuple, Optional, List

from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
import numpy as np


def extract_array(df, key='wap', stock_id_key='stock_id', default=1., stock_cnt=200):
    """
    Extracts key from all stocks. Expects only one time bucket
    :param df: One timeframe
    :param key:
    :param stock_id_key:
    :param default:
    :param stock_cnt:
    :return:
    """
    assert len(df) <= stock_cnt
    if len(df) == stock_cnt:
        return np.array(df[key])
    waps = np.full(stock_cnt, default)
    for index, row in df[[stock_id_key, key]].iterrows():
        waps[int(row[stock_id_key])] = row[key]
    return waps


class IndexPredictor:
    def __init__(self, weights=None):
        if weights is None:
            weights = np.array([0.004, 0.001, 0.002, 0.006, 0.004, 0.004, 0.002, 0.006, 0.006, 0.002, 0.002, 0.008, 0.006, 0.002,
                       0.008, 0.006, 0.002, 0.006, 0.004, 0.002, 0.004, 0.001, 0.006, 0.004, 0.002, 0.002, 0.004, 0.002,
                       0.004, 0.004, 0.001, 0.001, 0.002, 0.002, 0.006, 0.004, 0.004, 0.004, 0.006, 0.002, 0.002, 0.04,
                       0.002, 0.002, 0.004, 0.04, 0.002, 0.001, 0.006, 0.004, 0.004, 0.006, 0.001, 0.004, 0.004, 0.002,
                       0.006, 0.004, 0.006, 0.004, 0.006, 0.004, 0.002, 0.001, 0.002, 0.004, 0.002, 0.008, 0.004, 0.004,
                       0.002, 0.004, 0.006, 0.002, 0.004, 0.004, 0.002, 0.004, 0.004, 0.004, 0.001, 0.002, 0.002, 0.008,
                       0.02, 0.004, 0.006, 0.002, 0.02, 0.002, 0.002, 0.006, 0.004, 0.002, 0.001, 0.02, 0.006, 0.001,
                       0.002, 0.004, 0.001, 0.002, 0.006, 0.006, 0.004, 0.006, 0.001, 0.002, 0.004, 0.006, 0.006, 0.001,
                       0.04, 0.006, 0.002, 0.004, 0.002, 0.002, 0.006, 0.002, 0.002, 0.004, 0.006, 0.006, 0.002, 0.002,
                       0.008, 0.006, 0.004, 0.002, 0.006, 0.002, 0.004, 0.006, 0.002, 0.004, 0.001, 0.004, 0.002, 0.004,
                       0.008, 0.006, 0.008, 0.002, 0.004, 0.002, 0.001, 0.004, 0.004, 0.004, 0.006, 0.008, 0.004, 0.001,
                       0.001, 0.002, 0.006, 0.004, 0.001, 0.002, 0.006, 0.004, 0.006, 0.008, 0.002, 0.002, 0.004, 0.002,
                       0.04, 0.002, 0.002, 0.004, 0.002, 0.002, 0.006, 0.02, 0.004, 0.002, 0.006, 0.02, 0.001, 0.002,
                       0.006, 0.004, 0.006, 0.004, 0.004, 0.004, 0.004, 0.002, 0.004, 0.04, 0.002, 0.008, 0.002, 0.004,
                       0.001, 0.004, 0.006, 0.004])
        self.weights = weights
        self.stock_cnt = len(self.weights)

    def predict_index(self, df, wap_key='wap', stock_id_key='stock_id'):
        """
        :param df: Dataframe of one day at one time bucket
        :param wap_key:
        :param stock_id_key:
        :return:
        """
        waps = extract_array(df, wap_key, stock_id_key)
        return self.predict_index_arr(waps)

    def predict_index_arr(self, waps):
        res = self.weights @ waps
        return res

    def predict_target_arr(self, waps_now, waps_future):
        index = self.predict_index_arr(waps_now)
        index_future = self.predict_index_arr(waps_future)
        target = ((waps_future / waps_now) - (index_future / index)) * 10_000
        return target

    def predict_target_df(self, df_now, df_future, wap_key='wap', stock_id_key='stock_id'):
        wap = extract_array(df_now, wap_key, stock_id_key)
        wap_future = extract_array(df_future, wap_key, stock_id_key)
        return self.predict_target_arr(wap, wap_future)


def print_timeseries(rows, cols, data, predicted=None):
    figure, axs = plt.subplots(nrows=rows, ncols=cols)
    for row in range(rows):
        for col in range(cols):
            day = row * cols + col
            ax = axs[row][col]
            ax.plot(data[day])
            if predicted is not None:
                ax.plot(predicted[day], color='orange')
            ax.set_title(f"Day: {day}")
            if col != 0:
                ax.get_yaxis().set_visible(False)
            ax.get_xaxis().set_visible(False)
            ax.set_ylim([.995, 1.005])


def plot_timeseries_df(rows, cols, df, stock_id, features_to_show: List[Tuple[str, Optional[str]]]):
    figure, axs = plt.subplots(nrows=rows, ncols=cols)
    for row in range(rows):
        for col in range(cols):
            day = row * cols + col
            ax = axs[row][col]
            for feature_name, color in features_to_show:
                ax.plot(df[(df['date_id'] == day) & (df['stock_id'] == stock_id)][feature_name], color=color,
                        label=feature_name)
            ax.set_title(f"Day: {day}")
            ax.get_xaxis().set_visible(False)
            if row == 0 and col == 0:
                ax.legend()
