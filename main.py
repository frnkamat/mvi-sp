from sklearn.linear_model import LinearRegression
import utilities as u
import optiver2023
import pandas as pd

env = optiver2023.make_env()
iter_test = env.iter_test()

ip = u.IndexPredictor()

counter = 0
for (test, revealed_targets, sample_prediction) in iter_test:
    wap = u.extract_array(test, key='wap', stock_cnt=ip.stock_cnt)
    wap60 = wap  # TODO PREDICT
    # index = ip.predict_index_arr(test)
    preds = ip.predict_target_arr(wap, wap60)
    sample_prediction['target'] = preds

    env.predict(sample_prediction)
    counter += 1
